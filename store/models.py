from django.db import models
from django.urls import reverse

# Create your models here.

class Item(models.Model):
    name = models.CharField(max_length=950)
    image = models.CharField(max_length=9999999)
    description = models.TextField()
    category = models.CharField(max_length=300)
    price = models.DecimalField(max_digits=8, decimal_places=2)
    reviews = models.IntegerField()
    stock = models.IntegerField()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('item', args=[str(self.id)])