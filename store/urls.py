#store/urls.py

from django.urls import path

from .views import MainView, SearchView, ItemView, NewItemView, EditItemView, DeleteItemView


urlpatterns=[
    path('product/delete/<int:pk>', DeleteItemView.as_view(), name="delete_item"),
    path('product/edit/<int:pk>', EditItemView.as_view(), name="edit_item"),
    path('product/new', NewItemView.as_view(), name="new"),
    path('product/<int:pk>', ItemView.as_view(), name='item'),
    path('results/<search_result>/', SearchView.as_view(), name='search'),
    path('', MainView.as_view(), name='home'),
    
]