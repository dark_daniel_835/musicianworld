from django.test import TestCase
from  django.urls import reverse

from .models import Item
# Create your tests here.

class ItemModelTest(TestCase):
    def setUp(self):
        self.item = Item.objects.create(
            name = 'un instrumento',
            image = 'imagen1',
            description = 'descripcion del instrumento',
            category = 'categoria1',
            price = 123.45,
            reviews = 123,
            stock = 12
        )
    
    def test_string_representation(self):
        item = Item(name='instrumento de prueba')
        self.assertEqual(str(item), item.name)

    def test_get_absolute_url(self):
        self.assertEqual(self.item.get_absolute_url(), '/product/1')

    def test_item_content(self):
        self.assertEqual(f'{self.item.name}', 'un instrumento')
        self.assertEqual(f'{self.item.image}', 'imagen1')
        self.assertEqual(f'{self.item.description}', 'descripcion del instrumento')
        self.assertEqual(f'{self.item.category}', 'categoria1')
        self.assertEqual(f'{self.item.price}', '123.45')
        self.assertEqual(f'{self.item.reviews}', '123')
        self.assertEqual(f'{self.item.stock}', '12')

    def test_item_list_view(self):
        response = self.client.get(reverse('search', args=['categoria1']))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'imagen1')
        self.assertTemplateUsed(response, 'search.html')

    def test_item_detail_view(self):
        response = self.client.get('/product/1')
        no_response = self.client.get('/product/999999999/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'descripcion del instrumento')
        self.assertTemplateUsed(response, 'item.html')

    def test_post_create_view(self):
        response = self.client.post(reverse('new'), {
            'name' : 'nuevo instrumento',
            'image' : 'nueva imagen1',
            'description' : 'nueva descripcion del instrumento',
            'category' : 'nueva categoria1',
            'price' : 678.90,
            'reviews' : 678,
            'stock' : 67,
        })
        self.assertEqual(response.status_code, 302)
        
    def test_item_update_view(self):
        response = self.client.post(reverse('edit_item', args='1'), {
            'image' : 'imagen1 Actualizada',
            'price' : 600.00,
            'reviews' : 600,
            'stock' : 60
        })

        self.assertEqual(response.status_code, 302)

    def test_item_delete_view(self):
        response = self.client.get(
            reverse('delete_item', args='1')
        )
        self.assertEqual(response.status_code, 200)