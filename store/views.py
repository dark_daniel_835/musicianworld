from django.views.generic.base import TemplateView
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from .models import Item
from django.db.models import Q
from django.urls import reverse_lazy

# Create your views here.

class MainView(TemplateView):
    template_name = 'home.html'

class SearchView(ListView):
    model = Item
    template_name = 'search.html'

    def get_queryset(self):
        objects = super().get_queryset()
        return objects.filter(Q(category__exact = self.kwargs['search_result']) | Q(name__icontains = self.kwargs['search_result']))

class ItemView(DetailView):
    model = Item
    template_name = 'item.html'

class NewItemView(CreateView):
    model = Item
    template_name = 'add_item.html'
    fields='__all__'

class EditItemView(UpdateView):
    model = Item
    template_name = 'edit_item.html'
    fields=['image', 'price', 'reviews', 'stock']

class DeleteItemView(DeleteView):
    model = Item
    template_name = 'delete_item.html'
    success_url = reverse_lazy('home')